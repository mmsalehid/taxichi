from django.db import models

# Create your models here.



class Trip(models.Model):
    min_price = models.PositiveIntegerField()
    min_price_taxi = models.CharField(max_length = 100)
    min_response_time = models.FloatField()
    min_response_time_taxi = models.CharField(max_length = 100)
    trip_time = models.DateTimeField(auto_now_add=True)


