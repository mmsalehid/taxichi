from celery.task.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from .utils import get_taxi

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/10')),
    name="get_taxi_task",
    ignore_result=True
)
def get_taxi_task():
    get_taxi()




    


