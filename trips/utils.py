import requests
import json
import threading
from trips.models import Trip



def get_taxi_tap30(results,result_number):
    url = "https://tap33.me/api/v2/ride/preview"

    payload = "{\n    \"destinations\": [\n        {\n            \"latitude\": 35.758495,\n            \"longitude\": 51.44255\n        }\n    ],\n    \"hasReturn\": false,\n    \"origin\": {\n        \"latitude\": 35.75546,\n        \"longitude\": 51.416874\n    }\n}"
    headers = {
        'x-Authorization': "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjo2NzIzLCJwcm9maWxlSWQiOjY3MjMsInVzZXJQYXNzQ3JlZGVudGlhbElkIjo2NDA1LCJpc0Nob3NlbkZvckluY2VudGl2ZSI6bnVsbCwiZGV2aWNlVG9rZW4iOiJkdDJsRXFyM3RDUTpBUEE5MWJHeFpTSThpRWg0SkhyRG5jVE0zcDVnRXFjbHFaSl9tM3hLZ2JDYUJ0RHhQVzZpQm5OV2dXS2dQMks4UTUyN2VPQWFfSjJFelpPMGl5dGJkZGhvRV9YOXZjRTdzYXRxRm1mQnlobVNCQWFBZ3BDdmY5OWpiTlFDSWVjVFV2UExTSzNuQ19xRSIsImRldmljZVR5cGUiOiJBTkRST0lEIiwicmVmZXJyYWxDb2RlIjoiNzIwMjMiLCJyZWZlcnJlcklkIjpudWxsLCJyb2xlIjoiUEFTU0VOR0VSIiwiY3JlYXRlZEF0IjoiMjAxNi0wNi0wOVQxMDo1Mjo0NS43MDhaIiwidXBkYXRlZEF0IjoiMjAxNy0xMS0yM1QxMjoxODoyNC43MDVaIiwicHVzaHlEZXZpY2VUb2tlbiI6bnVsbCwidGVsZWdyYW1JZCI6NjQ5OTI1MzEsImZjbURldmljZVRva2VuIjoiZHQybEVxcjN0Q1E6QVBBOTFiR3haU0k4aUVoNEpIckRuY1RNM3A1Z0VxY2xxWkpfbTN4S2diQ2FCdER4UFc2aUJuTldnV0tnUDJLOFE1MjdlT0FhX0oyRXpaTzBpeXRiZGRob0VfWDl2Y0U3c2F0cUZtZkJ5aG1TQkFhQWdwQ3ZmOTlqYk5RQ0llY1RVdlBMU0szbkNfcUUiLCJyZWdpc3RyYXRpb25Db21wbGV0ZSI6dHJ1ZSwiY2l0eSI6IlRFSFJBTiIsImFjY0ZyZWVJZCI6bnVsbCwicHJvZmlsZSI6eyJpZCI6NjcyMywiZmlyc3ROYW1lIjoi2KLYsdmF2KfZhiIsImxhc3ROYW1lIjoi2KfYsdio2KfYqCDYstin2K_ZhyIsImlzTWFsZSI6dHJ1ZSwicGljdHVyZUlkIjoxNjY4NCwiYmFua2luZ0luZm9JZCI6bnVsbCwicmF0ZSI6MTAsImVtYWlsIjoiYXJtYW5AYXJiYWJ6YWRlaC5jb20iLCJjcmVhdGVkQXQiOiIyMDE2LTA2LTA5VDEwOjUyOjQ1LjcxMVoiLCJ1cGRhdGVkQXQiOiIyMDE3LTExLTI5VDA3OjIxOjE4LjY4MloiLCJlbWFpbFZlcmlmaWVkIjpudWxsLCJwaG9uZU51bWJlciI6Iis5ODkzNjQ1NDQwMTQiLCJzc24iOm51bGwsInNob3dSYXRlQXBwIjpmYWxzZSwiY29uZmlybWF0aW9uQ29kZSI6IjY5MjIyIiwiY29uZmlybWF0aW9uVmFsaWQiOmZhbHNlLCJjb25maXJtYXRpb25UcnkiOjAsImxhbmd1YWdlIjpudWxsLCJzZWNvbmRhcnlQaG9uZU51bWJlciI6bnVsbH0sInJlZ2lzdGVyZWQiOnRydWV9LCJpYXQiOjE1MTE5NDAwNzgsImF1ZCI6ImRvcm9zaGtlOmFwcCIsImlzcyI6ImRvcm9zaGtlOnNlcnZlciIsInN1YiI6ImRvcm9zaGtlOnRva2VuIn0.NJDLi1rYa7yovuPHrTSYGyEqa5k0qorLm31kx33bJnzpAW1URl0RPuclvH-nHo8hqWkEeVvt7oVC6BiSigncEA",
        'Accept-Language': "FA",
        'Accept-Encoding': "gzip",
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': "cf86b2c0-6a38-419e-ba8a-7c6eb0e66823"
        }

    response = requests.request("POST", url, data=payload, headers=headers)
    response_time = response.elapsed.total_seconds()
    response = json.loads(response.text)
    taxi = "tap30"
    price = int(response["data"]["priceInfos"][0]["price"])
    taxi_data ={
        "taxi" : taxi,
        "price" : price,
        "response_time" : response_time
    }
    results[result_number] = taxi_data


def get_taxi_dinng(results,result_number):
    url = "https://dnnng.com/includes/calculate.fast.php"

    payload = "latlng1=35.7330491321986,51.3725960254669&latlng2=35.7902516995282,51.435289978981&latlng3=0&returned=0&pause=0&status=1&in_order=0&target_time="
    headers = {
        'Content-Type': "application/x-www-form-urlencoded"
    }

    response = requests.post( url, data=payload, headers=headers)

    response_time = response.elapsed.total_seconds()
    response = json.loads(response.text)
    taxi = "dinnng"
    price = int(response["general"])
    taxi_data ={
        "taxi" : taxi,
        "price" : price,
        "response_time" : response_time
    }
    results[result_number] = taxi_data

NUMBER_OF_REQUESTS = 2

def get_taxi():
    results = [None for i in range(NUMBER_OF_REQUESTS)]
    tap30_thread = threading.Thread(target = get_taxi_tap30,args=(results,0))
    dinng_thread = threading.Thread(target = get_taxi_dinng,args=(results,1))
    tap30_thread.start()
    dinng_thread.start()
    tap30_thread.join()
    dinng_thread.join()
    min_price = results[0]["price"]
    min_price_taxi = results[0]["taxi"]
    min_response_time = results[0]["response_time"]
    min_response_taxi = results[0]["taxi"]
    if results[0]["price"] > results[1]["price"]:
        min_price = results[1]["price"]
        min_price_taxi = results[1]["taxi"]
    if results[0]["response_time"] > results[1]["response_time"]:
        min_response_time = results[1]["response_time"]
        min_response_taxi = results[1]["taxi"]
    new_trip = Trip(min_price=min_price,
                    min_price_taxi=min_price_taxi,
                    min_response_time=min_response_time,
                    min_response_time_taxi=min_response_taxi)
    new_trip.save()